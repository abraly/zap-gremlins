Sublime Text 2 ZapGremlins plug-in

Sublime Text 2 plug-in for "zapping Gremlins", such as in TextWrangler.

Mac Key Command: Press Cmd + Ctrl + z

If selection(s) are made, Zap happens only on selections.
If no selection is made, Zap happens to entire document.

Developed for Mac only, but might work in Win/Linux.