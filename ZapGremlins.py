import sublime, sublime_plugin

s = sublime.load_settings("ZapGremlins.sublime-settings")

class ZapgremlinsCommand(sublime_plugin.TextCommand):
	def run(self, edit):
		run_on_selections(self.view, edit, removeUnwanted)

def run_on_selections(view, edit, func):

	for region in view.sel():
		# If no selection, use the entire file as the selection
		if region.empty():
			region = sublime.Region(0, view.size())

		text = view.substr(region)
		view.replace(edit, region, func(text))

def removeUnwanted(text):

	character_replacements = [
		( u'\u2018', u"'"),   # LEFT SINGLE QUOTATION MARK
		( u'\u2019', u"'"),   # RIGHT SINGLE QUOTATION MARK
		( u'\u201c', u'"'),   # LEFT DOUBLE QUOTATION MARK
		( u'\u201d', u'"'),   # RIGHT DOUBLE QUOTATION MARK
		( u'\u201e', u'"'),   # DOUBLE LOW-9 QUOTATION MARK
		( u'\u00A0', u' '),   # NON BREAKING SPACE
		( u'\u2003', u' '),   # EM SPACE
		( u'\u2002', u' '),   # EN SPACE
		( u'\u2013', u'-'),   # EN DASH
		( u'\u2014', u'-'),   # EM DASH
		( u'\u2212', u'-'),   # MINUS
		( u'\u00AD', u'-'),   # SOFT HYPHEN
		( u'\u2010', u'-'),   # HYPHEN
		( u'\u2011', u'-'),   # NON BREAKING HYPHEN
		( u'\u2043', u'-'),   # HYPHEN BULLET
		( u'\u2026', u'...'), # HORIZONTAL ELLIPSIS
		( u'\u0152', u'OE'),  # LATIN CAPITAL LIGATURE OE
		( u'\u0153', u'oe')   # LATIN SMALL LIGATURE OE
	]
	for (undesired_character, safe_character) in character_replacements:
		text = text.replace(undesired_character, safe_character)

	return text